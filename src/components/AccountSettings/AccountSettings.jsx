//import liraries
import React, { useState } from "react";

import "./AccountSettings.css";
import { TextField, Button, makeStyles } from "@material-ui/core";
import { Avatar } from "@material-ui/core";
import * as firebase from "firebase/app";
import "firebase/storage";

const useStyles = makeStyles((theme) => ({
  large: {
    width: theme.spacing(10),
    height: theme.spacing(10),
  },
}));

// create a component named AccountSettings
const AccountSettings = (props) => {
  const { userState: user, profilePicURL } = props;
  const classes = useStyles();
  const [state, setState] = useState({
    name: user.displayName,
    email: user.email,
    photo: profilePicURL,
    newPhotoURL: null,
  });

  const handleChange = (e) =>
    setState({
      ...state,
      [e.target.name]: e.target.value,
    });

  const onPic = (e) => {
    const picRef = e.target.files[0];

    const reader = new FileReader();
    reader.readAsDataURL(picRef);

    reader.onload = (e) => {
      setState({
        ...state,
        photo: e.target.result,
        newPhotoURL: picRef,
      });
    };
  };

  const handleSubmit = () => {
    if (state.newPhotoURL) {
      const storageRef = firebase.storage().ref();
      const metadata = { contentType: state.newPhotoURL.type };
      const locationUploaded = `profile-pictures/${state.newPhotoURL.name}`;
      const upload = storageRef
        .child(locationUploaded)
        .put(state.newPhotoURL, metadata);

      upload
        .then((snapshot) => {
          const url = snapshot.ref.location.path;
          user.updateProfile({
            displayName: state.name,
            email: state.email,
            photoURL: url,
          });
        })
        .catch((err) => console.log(err));
    } else {
      user.updateProfile({
        displayName: state.name,
        email: state.email,
      });
    }
  };

  return (
    <aside className="account-settings">
      <section className="prof-pic">
        {" "}
        <Avatar alt="Aaron Toth" src={state.photo} className={classes.large} />
        <input
          type="file"
          name="avatar"
          accept="image/png, image/jpeg"
          onChange={onPic}
        />
      </section>
      <section className="prof-text">
        <section>
          Name:{" "}
          <TextField
            name="name"
            label={state.name}
            onChange={handleChange}
            onFocus={(e) => (e.target.value = state.name)}
            onBlur={(e) => (e.target.value = null)}
          />
        </section>
        <section>
          E-mail:{" "}
          <TextField
            name="email"
            label={state.email}
            onChange={handleChange}
            onFocus={(e) => (e.target.value = state.email)}
            onBlur={(e) => (e.target.value = null)}
          />
        </section>
      </section>
      <section className="submit">
        <Button onClick={handleSubmit} variant="contained" color="primary">
          Save Changes
        </Button>
      </section>
    </aside>
  );
};

//make this component available to the app
export default AccountSettings;
