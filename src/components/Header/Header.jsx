//import liraries
import React from "react";
import * as firebase from "firebase/app";
import { Avatar, Button } from "@material-ui/core";
import {
  AccountCircleOutlined,
  NotificationsNoneOutlined,
  MoreVertOutlined,
} from "@material-ui/icons";
import { makeStyles } from "@material-ui/core/styles";

import "./Header.css";
import { useRouteMatch, Link } from "react-router-dom";
import * as ROUTES from "../../constants/routes";

const useStyles = makeStyles((theme) => ({
  large: {
    width: theme.spacing(10),
    height: theme.spacing(10),
  },
}));

// create a component named Header
const Header = (props) => {
  const { setUserState, profPic } = props;
  const { url } = useRouteMatch();
  const classes = useStyles();
  const auth = firebase.auth();
  const user = auth.currentUser;

  const handleSignOut = () => {
    auth.signOut().then(() => {
      setUserState(null);
    });
  };

  return (
    <header>
      <div id="notification">
        <NotificationsNoneOutlined />
        <div className="notification view">
          <div className="title">Notifications</div>
        </div>
      </div>
      <div id="profile">
        <AccountCircleOutlined />
        <div className="profile view">
          <div className="account">
            <Avatar alt="Aaron Toth" src={profPic} className={classes.large} />
            <div className="name">{user.displayName || "Sample User"}</div>
          </div>
          <div className="link">
            <Link to={url + ROUTES.ACCOUNT}>My Profile</Link>
          </div>
          <div className="link">
            <Link to={url + ROUTES.ACCOUNT_SETTINGS}>Account Settings</Link>
          </div>
          <div className="sign-out">
            <Button
              color="primary"
              variant="contained"
              onClick={handleSignOut}
              style={{ backgroundColor: "#af0b19" }}
            >
              Sign out
            </Button>
          </div>
        </div>
      </div>
      <div id="more">
        <MoreVertOutlined />
        <div className="more view">
          <div className="title">Chat</div>
        </div>
      </div>
    </header>
  );
};

//make this component available to the app
export default Header;
