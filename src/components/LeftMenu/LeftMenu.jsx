//import liraries
import React from "react";
import {
  HomeOutlined,
  EventOutlined,
  PowerSettingsNewOutlined,
  CreateOutlined,
  SettingsOutlined,
} from "@material-ui/icons";
import { useRouteMatch, Link } from "react-router-dom";

import "./LeftMenu.css";
import * as ROUTES from "../../constants/routes";

// create a component named LeftMenu
const LeftMenu = () => {
  let { path } = useRouteMatch();
  return (
    <aside className="left-menu">
      <div className="page-title">Home IoT</div>
      <div className="menu-title">Application</div>
      <Link to={ROUTES.HOME}>
        <HomeOutlined /> <span>Home</span>
      </Link>
      <Link to={path + ROUTES.CALENDAR}>
        <EventOutlined /> <span>Calendar</span>
      </Link>
      <Link to={path + ROUTES.SWITCHES}>
        <PowerSettingsNewOutlined /> <span>Swithces</span>
      </Link>
      <Link to={path + ROUTES.WHITEBOARD}>
        <CreateOutlined /> <span>Whiteboard</span>
      </Link>
      <div className="menu-title">Other</div>
      <Link to={path + ROUTES.SETTINGS}>
        <SettingsOutlined /> <span>Settings</span>
      </Link>
    </aside>
  );
};

//make this component available to the app
export default LeftMenu;
