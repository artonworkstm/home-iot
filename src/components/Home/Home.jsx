//import liraries
import React from "react";
import Header from "../Header/Header";
import { Switch, Route, useRouteMatch, Redirect } from "react-router-dom";

import * as ROUTES from "../../constants/routes";
import "./Home.css";
import Account from "../Account/Account";
import AccountSettings from "../AccountSettings/AccountSettings";
import Settings from "../Settings/Settings";
import Admin from "../Admin/Admin";
import LeftMenu from "../LeftMenu/LeftMenu";
import Calendar from "../Calendar/Calendar";

// create a component named Home
const Home = (props) => {
  const { userState, setUserState, profilePicURL } = props;
  const { path } = useRouteMatch();

  if (userState) {
    return (
      <main>
        <Header setUserState={setUserState} profPic={profilePicURL} />
        <LeftMenu />
        <Switch>
          <Route path={path + ROUTES.CALENDAR} component={Calendar} />
          <Route path={path + ROUTES.ACCOUNT} component={Account} />
          <Route
            path={path + ROUTES.ACCOUNT_SETTINGS}
            component={() => <AccountSettings {...props} />}
          />
          <Route path={path + ROUTES.SETTINGS} component={Settings} />
          <Route path={path + ROUTES.ADMIN} component={Admin} />
        </Switch>
      </main>
    );
  } else {
    return <Redirect to="/signin" />;
  }
};

//make this component available to the app
export default Home;
