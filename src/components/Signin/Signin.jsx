//import liraries
import React, { useState } from "react";
import * as firebase from "firebase/app";
import { TextField, Button, Link } from "@material-ui/core";

import "./Signin.css";
import { Redirect } from "react-router-dom";

// create a component named Signin
const Signin = (props) => {
  const { userState } = props;
  if (!userState) {
    return (
      <section className="signin">
        <h1>Sign in</h1>
        <SigninForm {...props} />
      </section>
    );
  } else {
    return <Redirect to="/home" />;
  }
};

const SigninForm = (props) => {
  const { setUserState } = props;
  const [state, setState] = useState({
    email: "",
    pass: "",
  });

  const handleSubmit = () => {
    const auth = firebase.auth();
    const promise = auth.signInWithEmailAndPassword(state.email, state.pass);

    promise.then((e) => e && setUserState(e)).catch((err) => console.log(err));
  };

  const onEmail = (e) => {
    setState({
      ...state,
      email: e.target.value,
    });
  };

  const onPass = (e) => {
    setState({
      ...state,
      pass: e.target.value,
    });
  };

  return (
    <form className="signin-form" noValidate>
      <TextField variant="outlined" label="E-mail" onChange={onEmail} />
      <TextField variant="outlined" label="Password" onChange={onPass} />
      <Button
        variant="contained"
        color="primary"
        style={{ backgroundColor: "#2d6" }}
        onClick={handleSubmit}
      >
        Login
      </Button>
      <section>
        <Link href="/signup">Don't have an account? Sign up </Link>
      </section>
    </form>
  );
};

//make this component available to the app
export default Signin;
