//import liraries
import React from "react";
import {
  TextField,
  OutlinedInput,
  Button,
  Link,
  InputAdornment,
  IconButton,
  InputLabel,
  FormControl,
} from "@material-ui/core";
import * as firebase from "firebase/app";
import { Visibility, VisibilityOff } from "@material-ui/icons";

import "./Signup.css";
import { useHistory } from "react-router-dom";
import { Redirect } from "react-router-dom";

// create a component named Signup
const Signup = (props) => {
  const { userState } = props;

  if (!userState) {
    return (
      <section className="signup">
        <h1>Signup</h1>
        <SignupForm {...props} />
      </section>
    );
  } else {
    return <Redirect to="/home" />;
  }
};

const SignupForm = (props) => {
  const history = useHistory();
  const { setUserState } = props;
  const [state, setState] = React.useState({
    firstName: "",
    lastName: "",
    email: "",
    pass: "",
    passwordReenter: "",
    showPw: false,
  });

  const handleClickShowPassword = () => {
    setState({
      ...state,
      showPw: !state.showPw,
    });
  };

  const handleSubmit = () => {
    const auth = firebase.auth();
    const promise = auth.createUserWithEmailAndPassword(
      state.email,
      state.pass
    );

    promise
      .then((e) => {
        let user, name, photoURL;
        name = `${state.firstName} ${state.lastName}`;
        photoURL = "profile-pictures/profile-pic-man.jpeg";
        if (e) {
          user = auth.currentUser;
          user.updateProfile({
            displayName: name,
            photoURL: photoURL,
          });

          setUserState(e);
          history.push("/home");
        }
      })
      .catch((err) => console.log(err));
  };

  // TODO: Validate e-mail
  const onEmail = (e) => {
    setState({
      ...state,
      email: e.target.value,
    });
  };

  // TODO: Validate pass and rePass
  const onPass = (e) => {
    setState({
      ...state,
      pass: e.target.value,
    });
  };

  const onFirstName = (e) => {
    setState({
      ...state,
      firstName: e.target.value,
    });
  };

  const onLastName = (e) => {
    setState({
      ...state,
      lastName: e.target.value,
    });
  };

  return (
    <form className="signup-form">
      <section>
        <TextField
          variant="outlined"
          label="First Name"
          onChange={onFirstName}
        />
        <TextField variant="outlined" label="Last Name" onChange={onLastName} />
      </section>
      <TextField
        variant="outlined"
        label="E-mail"
        type="email"
        onChange={onEmail}
      />
      <FormControl variant="outlined">
        <InputLabel htmlFor="password">Password</InputLabel>
        <OutlinedInput
          id="password"
          variant="outlined"
          label="Password"
          onChange={onPass}
          type={state.showPw ? "text" : "password"}
          endAdornment={
            <InputAdornment position="end">
              <IconButton
                aria-label="toggle password visibility"
                onClick={handleClickShowPassword}
                edge="end"
              >
                {state.showPw ? <Visibility /> : <VisibilityOff />}
              </IconButton>
            </InputAdornment>
          }
        />
      </FormControl>
      <FormControl variant="outlined">
        <InputLabel htmlFor="pw-reenter">Password</InputLabel>
        <OutlinedInput
          id="pw-reenter"
          variant="outlined"
          label="Password"
          type={state.showPw ? "text" : "password"}
          endAdornment={
            <InputAdornment position="end">
              <IconButton
                aria-label="toggle password visibility"
                onClick={handleClickShowPassword}
                edge="end"
              >
                {state.showPw ? <Visibility /> : <VisibilityOff />}
              </IconButton>
            </InputAdornment>
          }
        />
      </FormControl>
      <Button
        variant="contained"
        color="primary"
        style={{ backgroundColor: "#2d6" }}
        onClick={handleSubmit}
        className="signup-submit"
      >
        Sign up
      </Button>
      <section>
        <Link href="/signin">Already have an account? Sign in</Link>
      </section>
    </form>
  );
};

//make this component available to the app
export default Signup;
