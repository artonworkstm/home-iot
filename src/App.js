import React, { Component } from "react";
import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/storage";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import * as ROUTES from "./constants/routes";
import "./App.css";
import Landing from "./components/Landing/Landing";
import Signin from "./components/Signin/Signin";
import Signup from "./components/Signup/Signup";
import PwForget from "./components/PwForget/PwForget";
import Home from "./components/Home/Home";
import { firebaseConfig } from "./firebase";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      userState: null,
      profilePicURL: "null",
    };
  }

  componentDidMount() {
    const app = firebase.initializeApp(firebaseConfig);
    const auth = app.auth();

    auth.onAuthStateChanged((user) => {
      this.setState({ userState: user });
      if (user) {
        const storageRef = app.storage().ref();
        if (user.photoURL) {
          const pictureRef = storageRef.child(user.photoURL);

          pictureRef
            .getDownloadURL()
            .then((url) => {
              this.setState({
                profilePicURL: url,
              });
            })
            .catch((err) => console.log(err));
        }
      }
    });
  }

  setUserState = (e) => this.setState({ userState: e });

  render() {
    const { userState } = this.state;

    return (
      <Router>
        <section className="App">
          <Switch>
            <Route path={ROUTES.LANDING} exact component={Landing} />
            <Route
              path={ROUTES.SIGN_IN}
              component={() => (
                <Signin
                  userState={userState}
                  setUserState={this.setUserState}
                />
              )}
            />
            <Route
              path={ROUTES.SIGN_UP}
              component={() => (
                <Signup
                  userState={userState}
                  setUserState={this.setUserState}
                />
              )}
            />
            <Route path={ROUTES.PASSWORD_FORGET} component={PwForget} />
            <Route
              path={ROUTES.HOME}
              component={() => (
                <Home {...this.state} setUserState={this.setUserState} />
              )}
            />
          </Switch>
        </section>
      </Router>
    );
  }
}

export default App;
